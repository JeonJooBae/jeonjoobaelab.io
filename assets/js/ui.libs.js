'use strict';

// 유틸들
window.DisplayUtils = {
  /**
   * 엘리먼트의 클래스 토글
   * @author Natasha
   * @param {html DOM} target 대상 엘리먼트
   * @param {String} str 특정 클래스명을 토글하려면 전달 (default 'on')
   * @returns target
   */
  toggleOn(target, str='on') {
    const flag = $(target).hasClass(str);
    // console.log(`toggle class : ${flag}`);
    $(target).toggleClass(str, !flag);
    return flag;
  },
};

/**
 * 전역 Alert
 * @ahthor Natasha
 * @usage Alert.show(<String> TITLE, <String> MESSAGE);
 * @return {Object<function>} center, show, hide
 */
window.Alert = (function($) {
  let $element;
  let callback;

  /**
   * 알림창 열기
   * @public
   * @param title 창제목
   * @param text 알림 내용
   * @param fn 버튼 콜백
   */
  function _show(title, text, fn) {
    callback = fn;

    $element
      .find('.msg').html(text).end()
      .dialog('option', 'title', title)
      .dialog('open');
  };

  /**
   * 알림창 닫기
   * @public
   */
   function _hide() {
    callback = null;
    $element.dialog('close');
  };

  /**
   * 알림창 위치응 window 의 가운데로 (resize 이벤트에서 호출하면 좋다)
   */
   function _center() {
    $element.dialog("option", "position", {
      my: "center",
      at: "center",
      of: window
    });
  };

  const _setup = function() {
    const element = _create();

    $element = $(element).dialog({
      dialogClass: "modal-alert mini",
      modal: true,
      show: 'fade',
      width: 340,
      draggable: false,
      resizable: false,
      autoOpen: false,
      buttons: [
        {
          text: '확인',
          class: 'btn ok',
          click: _functionOk
        },
      ]
    });
  };

  function _create() {
    const dom = document.createElement('div');
    const child = `<div class="msg"></div>`
    $(dom).append(child);
    return dom;
  };

  function _functionOk() {
    if(callback != undefined) {
      callback();
    }
    _hide();
  }

  _setup();

  return {
    center: _center,
    show: _show,
    hide: _hide
  }
}(jQuery)); // Alert


/**
 * 전역 Confirm
 * @ahthor Natasha
 * @usage Confirm.show(<String> TITLE, <String> MESSAGE, {null || 'negative'}, {ok: {<String> label, <function> fn}, no: {<String> label, <function>}});
 * @return {Object<function>} center, show, hide
 */
window.Confirm = (function($) {
  let $element;
  let callback = null;

  /**
   * 알림창 열기
   * @public
   * @param title 창제목
   * @param text 알림 내용
   * @param theme 알림창 테마 (null || 'negative')
   * @param fn 버튼 레이블, 콜백
   */
  function _show(title, text, theme, fn) {

    callback = fn;

    const okLabel = callback?.ok?.label || '확인';
    const noLabel = callback?.no?.label || '취소';

    $element
    .dialog('widget')
    .find('.btn.ok').text(okLabel).end()
    .find('.btn.no').text(noLabel);

    $element
      .find('.msg').html(text).end()
      .find('.title').html(title).end()
      .dialog('option', 'dialogClass', `modal-confirm mini-info ${theme || ''}`)
      .dialog('open');
  };

  /**
   * 알림창 닫기
   * @public
   */
  function _hide() {
    callback = null;
    $element.dialog('close');
  };

  /**
   * 알림창 위치응 window 의 가운데로 (resize 이벤트에서 호출하면 좋다)
   */
   function _center() {
    $element.dialog("option", "position", {
      my: "center",
      at: "center",
      of: window
    });
  };

  const _setup = function() {
    const element = _create();

    $element = $(element).dialog({
      dialogClass: "modal-confirm mini-info",
      modal: true,
      show: 'fade',
      width: 340,
      draggable: false,
      resizable: false,
      autoOpen: false,
      buttons: [
        {
          text: '취소',
          class: 'btn no',
          click: _functionNo
        },
        {
          text: '확인',
          class: 'btn ok',
          click: _functionOk
        },
      ]
    });
  };

  function _create() {
    const dom = document.createElement('div');
    const child = `
    <dl class="txt-message">
      <dt class="title">로그아웃</dt>
      <dd class="msg">로그아웃 하시겠습니까?​</dd>
    </dl>`

    $(dom)
    .prop('class', 'info-message')
    .append(child);
    return dom;
  };

  function _functionOk() {
    callback?.ok?.callback && callback.ok.callback();
  }
  function _functionNo() {
    callback?.no?.callback && callback.no.callback();
  }

  _setup();

  return {
    center: _center,
    show: _show,
    hide: _hide
  }
}(jQuery)); // Confirm


// UI 관리용 객체들
(function($) {
  /**
   * Collapsable 관리
   * 형제들과 상호작용을 하지 않고 독립적으로 펼치거나 접히는 UI
   * @author Natasha
   * @usage $('.ui-collapse').collapsable();
   */
  class Collapsable {
    constructor(element, speed=200) {
      this.element = element;
      this.$btn = $(element).children('.ui-collapse-text');
      this.$collapsable = $(element).children('.ui-collapsable');
      this.speed = speed;
		  // console.log("%cnew Collapsable --------------------", "color:purple;font-size: 14px;");
		  // console.log(this.element);
    }
    setup() {
      this.$btn.on('click', $.proxy(this.collapse, this));
    }
    open() {
        // console.log('collapse open');
        if(!$(this.element).hasClass("on")) {
          $(this.element).addClass('on');
        }
        this.$collapsable.slideDown(this.speed);
    }
    close() {
        // console.log('collapse close');
        if($(this.element).hasClass("on")) {
          $(this.element).removeClass('on');
        }
        this.$collapsable.slideUp(this.speed);
    }
    collapse(e) {
      e.stopPropagation();
      DisplayUtils.toggleOn(this.element) ? this.close() : this.open();
    }
  }

  Collapsable.NAME = 'Collapsable';
  Collapsable.CORE = {
    createInstance(element, option) {
      let instance = $(element).data(Collapsable.NAME);
      // console.log(instance);
      if(!instance) {
        instance = new Collapsable(element);
        instance.setup();
        instance.close(); //일단 접힘 상태에서 시작함
        $(element).data(Collapsable.NAME, instance);
      }

      // console.log(`option is ${option}`);
      if(typeof option === 'string') {
        instance[option]();
      }
      return instance;
    }
  };
  // Collapseable

  $.fn.collapsable = function(option) {
    $(this).each((i, el) => Collapsable.CORE.createInstance(el, option));
    return this;
  };

  /**
   * InputClear 관리
   * 입력요소에 삭제 버튼 UI
   * @author Arjun
   * @usage $('.form-input').inputClear();
   */
    class InputClear {
    constructor(element) {
      this.element = element;
      this.$inputElement = $(element).find('input[type="text"], input[type="search"], input[type="email"], input[type="password"], textarea');
      this.$clearBtn = $(element).find('.btn-input-clear');
    }
    setup() {
      this.$inputElement.on('keyup', $.proxy(this.inputClearHandler, this));
      this.$clearBtn.on('click', $.proxy(this.inputClearClickHandler, this));
      this.$inputElement.trigger('keyup');
    }
    clearBtnShow() {
      $(this.element).addClass('is-clear');
    }
    clearBtnHide() {
      $(this.element).removeClass('is-clear');
    }
    inputClearHandler(e) {
      e.stopPropagation();
      this.$inputElement.val().length;
      if(this.$inputElement.val().length > 0) {
        this.clearBtnShow();
      } else {
        this.clearBtnHide();
      }
    }
    inputClearClickHandler(e){
      e.preventDefault();
      e.stopPropagation();
      if(this.$inputElement.val().length > 0) {
        this.$inputElement.val('');
        this.$inputElement.trigger('keyup');
        this.$inputElement.focus();
      }
    }
  }

  InputClear.NAME = 'InputClear';
  InputClear.CORE = {
    createInstance(element, option) {
      if($(element).find('.btn-input-clear').length <= 0) return false;
      let instance = $(element).data(InputClear.NAME);
      if(!instance) {
        instance = new InputClear(element);
        instance.setup();
        $(element).data(InputClear.NAME, instance);
      }
      if(typeof option === 'string') {
        instance[option]();
      }
      return instance;
    }
  };
  // InputClear
  $.fn.inputClear = function(option) {
    $(this).each((i, el) => InputClear.CORE.createInstance(el, option));
    return this;
  };

}(jQuery));

// Toast Popup
!function(e){e(["jquery"],function(e){return function(){function t(e,t,n){return g({type:O.error,iconClass:m().iconClasses.error,message:e,optionsOverride:n,title:t})}function n(t,n){return t||(t=m()),v=e("#"+t.containerId),v.length?v:(n&&(v=d(t)),v)}function o(e,t,n){return g({type:O.info,iconClass:m().iconClasses.info,message:e,optionsOverride:n,title:t})}function s(e){C=e}function i(e,t,n){return g({type:O.success,iconClass:m().iconClasses.success,message:e,optionsOverride:n,title:t})}function a(e,t,n){return g({type:O.warning,iconClass:m().iconClasses.warning,message:e,optionsOverride:n,title:t})}function r(e,t){var o=m();v||n(o),u(e,o,t)||l(o)}function c(t){var o=m();return v||n(o),t&&0===e(":focus",t).length?void h(t):void(v.children().length&&v.remove())}function l(t){for(var n=v.children(),o=n.length-1;o>=0;o--)u(e(n[o]),t)}function u(t,n,o){var s=!(!o||!o.force)&&o.force;return!(!t||!s&&0!==e(":focus",t).length)&&(t[n.hideMethod]({duration:n.hideDuration,easing:n.hideEasing,complete:function(){h(t)}}),!0)}function d(t){return v=e("<div/>").attr("id",t.containerId).addClass(t.positionClass),v.appendTo(e(t.target)),v}function p(){return{tapToDismiss:!0,toastClass:"toast",containerId:"toast-container",debug:!1,showMethod:"fadeIn",showDuration:300,showEasing:"swing",onShown:void 0,hideMethod:"fadeOut",hideDuration:1e3,hideEasing:"swing",onHidden:void 0,closeMethod:!1,closeDuration:!1,closeEasing:!1,closeOnHover:!0,extendedTimeOut:1e3,iconClasses:{error:"toast-error",info:"toast-info",success:"toast-success",warning:"toast-warning"},iconClass:"toast-info",positionClass:"toast-bottom-center",timeOut:5e3,titleClass:"toast-title",messageClass:"toast-message",escapeHtml:!1,target:"body",closeHtml:'<button type="button">&times;</button>',closeClass:"toast-close-button",newestOnTop:!0,preventDuplicates:!1,progressBar:1,progressClass:"toast-progress",rtl:!1}}function f(e){C&&C(e)}function g(t){function o(e){return null==e&&(e=""),e.replace(/&/g,"&amp;").replace(/"/g,"&quot;").replace(/'/g,"&#39;").replace(/</g,"&lt;").replace(/>/g,"&gt;")}function s(){c(),u(),d(),p(),g(),C(),l(),i()}function i(){var e="";switch(t.iconClass){case"toast-success":case"toast-info":e="polite";break;default:e="assertive"}I.attr("aria-live",e)}function a(){E.closeOnHover&&I.hover(H,D),!E.onclick&&E.tapToDismiss&&I.click(b),E.closeButton&&j&&j.click(function(e){e.stopPropagation?e.stopPropagation():void 0!==e.cancelBubble&&e.cancelBubble!==!0&&(e.cancelBubble=!0),E.onCloseClick&&E.onCloseClick(e),b(!0)}),E.onclick&&I.click(function(e){E.onclick(e),b()})}function r(){I.hide(),I[E.showMethod]({duration:E.showDuration,easing:E.showEasing,complete:E.onShown}),E.timeOut>0&&(k=setTimeout(b,E.timeOut),F.maxHideTime=parseFloat(E.timeOut),F.hideEta=(new Date).getTime()+F.maxHideTime,E.progressBar&&(F.intervalId=setInterval(x,10)))}function c(){t.iconClass&&I.addClass(E.toastClass).addClass(y)}function l(){E.newestOnTop?v.prepend(I):v.append(I)}function u(){if(t.title){var e=t.title;E.escapeHtml&&(e=o(t.title)),M.append(e).addClass(E.titleClass),I.append(M)}}function d(){if(t.message){var e=t.message;E.escapeHtml&&(e=o(t.message)),B.append(e).addClass(E.messageClass),I.append(B)}}function p(){E.closeButton&&(j.addClass(E.closeClass).attr("role","button"),I.prepend(j))}function g(){E.progressBar&&(q.addClass(E.progressClass),I.prepend(q))}function C(){E.rtl&&I.addClass("rtl")}function O(e,t){if(e.preventDuplicates){if(t.message===w)return!0;w=t.message}return!1}function b(t){var n=t&&E.closeMethod!==!1?E.closeMethod:E.hideMethod,o=t&&E.closeDuration!==!1?E.closeDuration:E.hideDuration,s=t&&E.closeEasing!==!1?E.closeEasing:E.hideEasing;if(!e(":focus",I).length||t)return clearTimeout(F.intervalId),I[n]({duration:o,easing:s,complete:function(){h(I),clearTimeout(k),E.onHidden&&"hidden"!==P.state&&E.onHidden(),P.state="hidden",P.endTime=new Date,f(P)}})}function D(){(E.timeOut>0||E.extendedTimeOut>0)&&(k=setTimeout(b,E.extendedTimeOut),F.maxHideTime=parseFloat(E.extendedTimeOut),F.hideEta=(new Date).getTime()+F.maxHideTime)}function H(){clearTimeout(k),F.hideEta=0,I.stop(!0,!0)[E.showMethod]({duration:E.showDuration,easing:E.showEasing})}function x(){var e=(F.hideEta-(new Date).getTime())/F.maxHideTime*100;q.width(e+"%")}var E=m(),y=t.iconClass||E.iconClass;if("undefined"!=typeof t.optionsOverride&&(E=e.extend(E,t.optionsOverride),y=t.optionsOverride.iconClass||y),!O(E,t)){T++,v=n(E,!0);var k=null,I=e("<div/>"),M=e("<div/>"),B=e("<div/>"),q=e("<div/>"),j=e(E.closeHtml),F={intervalId:null,hideEta:null,maxHideTime:null},P={toastId:T,state:"visible",startTime:new Date,options:E,map:t};return s(),r(),a(),f(P),E.debug&&console&&console.log(P),I}}function m(){return e.extend({},p(),b.options)}function h(e){v||(v=n()),e.is(":visible")||(e.remove(),e=null,0===v.children().length&&(v.remove(),w=void 0))}var v,C,w,T=0,O={error:"error",info:"info",success:"success",warning:"warning"},b={clear:r,remove:c,error:t,getContainer:n,info:o,options:{},subscribe:s,success:i,version:"2.1.3",warning:a};return b}()})}("function"==typeof define&&define.amd?define:function(e,t){"undefined"!=typeof module&&module.exports?module.exports=t(require("jquery")):window.toastr=t(window.jQuery)});
