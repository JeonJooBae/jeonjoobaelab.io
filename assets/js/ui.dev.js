let SunhUi = {};
(function() {
  /**
   * 파일업로드
   * 게시판에 파일 업로드 버튼 UI
   * 참고페이지: M3_C3.html
   * @author Arjun
   * @usage SunhUi.filePondInit('<선택자>.my-pond', <Object> Option_1);
   */  
    SunhUi.filePondInit = (element, opt) => {
      $.fn.filepond.registerPlugin(FilePondPluginFileValidateSize);
      let rowColnum = 6;
      let defaultboxH = 120;
      let flieFondLIstStartNum = 0;
      let rootBox = $(element).closest('.form-file-upload-body');
      let sizeInfo = rootBox.find('.size');
      opt = opt ? opt : {};
      let setOption = {
          height: '120px',
          maxFileSize: '1000MB',
          styleButtonRemoveItemPosition: 'right',
          labelIdle: `<span class="upload-guide">첨부파일을 마우스로 끌어올려주세요. 파일은 최대 500MB까지 업로드 가능합니다.​</span>
                      <span class="filepond--label-action"><span class="blind">파일업로드</span></span>`,
          iconRemove:`<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M10 3.125C6.20304 3.125 3.125 6.20304 3.125 10C3.125 13.797 6.20304 16.875 10 16.875C13.797 16.875 16.875 13.797 16.875 10C16.875 6.20304 13.797 3.125 10 3.125ZM1.875 10C1.875 5.51269 5.51269 1.875 10 1.875C14.4873 1.875 18.125 5.51269 18.125 10C18.125 14.4873 14.4873 18.125 10 18.125C5.51269 18.125 1.875 14.4873 1.875 10Z" fill="#AEAEAE"/>
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M12.9419 7.05806C13.186 7.30214 13.186 7.69786 12.9419 7.94194L7.94194 12.9419C7.69786 13.186 7.30214 13.186 7.05806 12.9419C6.81398 12.6979 6.81398 12.3021 7.05806 12.0581L12.0581 7.05806C12.3021 6.81398 12.6979 6.81398 12.9419 7.05806Z" fill="#AEAEAE"/>
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M7.05806 7.05806C7.30214 6.81398 7.69786 6.81398 7.94194 7.05806L12.9419 12.0581C13.186 12.3021 13.186 12.6979 12.9419 12.9419C12.6979 13.186 12.3021 13.186 12.0581 12.9419L7.05806 7.94194C6.81398 7.69786 6.81398 7.30214 7.05806 7.05806Z" fill="#AEAEAE"/>
                      </svg>
                      `,
          beforeDropFile:function(file) {
            flieFondLIstStartNum = 0;
          }
      }
      $.extend(setOption, opt);
      $(element).filepond(setOption);
  
      let getTotalSize = (eventType) => {
        setTimeout(function() {
          let infoSub = $(element).find('.filepond--file-info-sub');
          let arrSizeVal = [];
          let totSizeVal = 0;
  
          if(infoSub.length <= 0) {
            totSizeVal = 0;
            sizeInfo.text('0.0 MB');
            return false; 
          }
          $.each(infoSub, function(idx, obj) {
            let txtSize = $(obj).text();
            let numSize = parseFloat(txtSize);
            if(txtSize.indexOf('bytes') >= 0) {
              let numMB_1 = numSize / 1048576;
              arrSizeVal.push(numMB_1);
              totSizeVal += numMB_1;
            } else if(txtSize.indexOf('KB') >= 0) {
              let numMB_2 = numSize / 1024;
              arrSizeVal.push(numMB_2);
              totSizeVal += numMB_2;
            } else {
              arrSizeVal.push(numSize);
              totSizeVal += numSize;
            }
          });
          sizeInfo.text(totSizeVal.toFixed(2) + ' MB')
        }, 500);
      }
  
      let guideShowHideCheck = (self,eventType) => {
        let filepondList = $('.filepond--list',self).find('li');
        let filepondListLen = filepondList.length;
        let uploadGuide = $(element).find('.upload-guide');
        if(eventType == 'addfile' && filepondListLen > 0) {
          uploadGuide.hide();
        } else if(filepondListLen - 1 == 0 && eventType == 'removefile'){
          uploadGuide.show();
          addRemoveCheck = 0;
        }
      }
  
      let filepondExtentionCheckFnc = (self,eventType) => {
          let filepondList = $('.filepond--list',self).find('li');
          guideShowHideCheck(self,eventType);
          getTotalSize('addfile');
          $.each(filepondList, (idx,obj) => {
            $(obj).filter(':contains(xls)').addClass('xls');
            $(obj).filter(':contains(xlsx)').addClass('xls');
            $(obj).filter(':contains(pdf)').addClass('pdf');
            $(obj).filter(':contains(jpg)').addClass('jpg');
            $(obj).filter(':contains(png)').addClass('png');
            $(obj).filter(':contains(gif)').addClass('gif');
            $(obj).filter(':contains(doc)').addClass('doc');
            $(obj).filter(':contains(zip)').addClass('zip');
  
            if($(obj).filter(':contains(xls)').length <= 0 &&
              $(obj).filter(':contains(xlsx)').length <= 0 &&
              $(obj).filter(':contains(pdf)').length <= 0 &&
              $(obj).filter(':contains(jpg)').length <= 0 &&
              $(obj).filter(':contains(png)').length <= 0 &&
              $(obj).filter(':contains(gif)').length <= 0 &&
              $(obj).filter(':contains(doc)').length <= 0 &&
              $(obj).filter(':contains(zip)').length <= 0) {
                $(obj).addClass('etc');
              }
          });
      }
      let boxHeightResize = (filepondListLen) => {
        let h = Math.ceil(filepondListLen / rowColnum) * 65;
          if(h <= defaultboxH) {
            h = defaultboxH;
          }
          rootBox.height(h);
      }
  
      $(element).on('FilePond:addfile', function(e) {
        let filepondList = $('.filepond--list',this).find('li');
        let filepondListLen = filepondList.length;
        flieFondLIstStartNum++;
        if(flieFondLIstStartNum == filepondListLen) {
          boxHeightResize(filepondListLen);
          filepondExtentionCheckFnc(this,'addfile');
        }
      });
  
      $(element).on('FilePond:removefile', function(e) {
        getTotalSize('removefile');
        guideShowHideCheck(this,'removefile');
        let filepondList = null;
        let filepondListLen = 0;
        setTimeout(()=>{
          filepondList = $('.filepond--list',this).find('li');
          filepondListLen = filepondList.length-1;
          boxHeightResize(filepondListLen);
        }, 500);
        if(filepondListLen == 0) {
          flieFondLIstStartNum = 0;
          return false;
        }
        flieFondLIstStartNum--;
      });
      rootBox.find('.file-uploaded-all-del').off('click');
      rootBox.find('.file-uploaded-all-del').on('click', function() {
        $(element).filepond('removeFiles');
        let fileUloadRoot = $(this).closest('.form-file-upload-body');
        let uploadGuide = fileUloadRoot.find('.upload-guide');
        uploadGuide.show();
        rootBox.height(defaultboxH);
        flieFondLIstStartNum = 0;
      });    
    } //filepond
  
    /**
     * SunhUi.boardPlay: 게시판 적용 함수
     * 게시판 jodit 적용 UI
     * 참고페이지: M3_C3.html
     * @author Arjun
     * @usage SunhUi.boardPlay(<선택자>'#editor', <옵션|생략가능> { });
     */  
     SunhUi.boardPlay = (element, obj) => {
      obj = obj ? obj : {}; 
      let setObj = {
        toolbarButtonSzie:"small",
        minHeight:383,
        minHeight:600,
        buttons: "source, bold, italic, underline, strikethrough, eraser, superscript, subscript, ul, ol"        
      }
      $.extend(setObj, obj);
      $(element).each((idx)=> {
        new Jodit(element, setObj);
      });
     }
  })();