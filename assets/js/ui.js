/**
 * ready
 * @requires /assets/js/ui.libs.js
 */
$(function() {
  // 토글 UI
  $('.ui-toggle').on('click', function() {
    DisplayUtils.toggleOn(this);
  });

  // 접히는 UI
  $('.ui-collapse')
    .collapsable()
    .filter('.collapse-open')
    .collapsable('open');

  // tree UI
  $('.js-tree--nav .js-tree')
    .jstree({
      "plugins" : [ "wholerow" ],
      'core' : {
        'themes' : { 'icons' : true }
      }
    })
    .bind("select_node.jstree", function(e, data) {
      // a 엘리먼트의 href 비활성
      e.stopPropagation();
      e.preventDefault();

      // a 엘리먼트의 href 활성
      // window.location.href = data.node.a_attr.href;
    });
  $('.js-tree--checkbox .js-tree').jstree({
    "plugins" : [ "wholerow", "checkbox" ],
    "checkbox" : {
      "keep_selected_style" : false
    },
    'core' : {
      'themes' : { 'icons' : true }
    }
  });
  $('.btn-tree.open').on('click', function(){
    const $tree = $(this).closest('[class*="js-tree--"]').find('.js-tree');
    $tree.jstree('open_all');
  });
  $('.btn-tree.close').on('click', function(){
    const $tree = $(this).closest('[class*="js-tree--"]').find('.js-tree');
    $tree.jstree('close_all');
  });
  //--- tree UI


  /* Modal */
  $(".modal").each(function(index) {
    if ($(this).length > 0){
      /**
       * Options
       * @author Boris
       */
      let modal = $(this).data('modal'),
          modalOpen = $(this).data('open'),
          modalDimmed = $(this).data('dimmed'),
          modalClass = $(this).data('class'),
          modalWidth = $(this).data('width'),
          modalEffect = $(this).data('effect'),
          position = $(this).data('position'),
          resizable = $(this).data('resize'),
          draggable = $(this).data('drag');

      // selector
      const modalPlugin = $('.'+modal);

      modalPlugin.dialog({
        autoOpen: modalOpen ? modalOpen : false,
        modal: (modalDimmed !== undefined) ? modalDimmed : true,
        show: modalEffect ? modalEffect : 'fade',
        resizable: resizable ? resizable : false,
        draggable: draggable ? draggable : false,
        width: modalWidth,
        classes: {
          "ui-dialog": modalClass
        }
      });
      // resize
      $(window).resize(function(){
        if (position){
          modalPlugin.dialog("option", "position", { my: "center", at: "center", of: window });
        }
      });

      // close
      modalPlugin.find(".page-btn .btn").on("click", function() {
        modalPlugin.dialog("close");
      });
    }
  });
  // opener
  $('.opener').each(function() {
    let opener = $(this).data('opener');
    $('.'+opener).on("click", function() {
      let remodal = opener.replace('opener','modal');
      $('.'+remodal).dialog("open");
    });
  })


  /* Tabs */
  $(".tabs").each(function() {
    /**
     * Options
     * @author Boris
     */
    let active = $(this).data('active');

    $(this).tabs({
      active: active ? active : 0
    });
  })

  /* Tooltip */
  $(".tooltip").each(function(e) {
    let classes = $(this).data('class') || '';
    $(this).tooltip({
      tooltipClass: classes,
      position: {
        my: classes ? "right+25 bottom-12" : "left-35 bottom-12",
        at: classes ? "center top" : "center top",
        using: function( position, feedback ) {
          $( this ).css( position );
          $( "<div>" )
            .addClass( "arrow" )
            .addClass( feedback.vertical )
            .addClass( feedback.horizontal )
            .appendTo( this );
        }
      },
      content: function() {
        return $(this).attr('title');
      }
    });
  });

  /* 전자결재 split */
  (function(){
    let _splitFlag = false;
    const $pageSplit = $('.page-split');

    $pageSplit.each(function(index) {
      const $btn = $(this).find('.btn-split');

      ($(this).hasClass('open'))
            ? ($btn.next().text('기본 보기'), _splitFlag = true)
            : ($btn.next().text('확대해서 보기'), _splitFlag = false);
      $btn.on('click', function(){
        $(this).toggleClass('open').parents('.page-split').toggleClass('open');
        (_splitFlag)
              ? ($(this).next().text('확대해서 보기'), _splitFlag = false)
              : ($(this).next().text('기본 보기'), _splitFlag = true);
      });
    })
  })();

  /*입력폼 Clear버튼*/
  $('.form-input').inputClear();

  // input:password 눈깔버튼
  $('.btn-show-pw').on('click', function(e) {
    e.preventDefault();

    const $ui = $(this).siblings('input');
    const flag = DisplayUtils.toggleOn(this, 'active');
    $ui.attr('type', flag ? 'password' : 'text');
  });
});
